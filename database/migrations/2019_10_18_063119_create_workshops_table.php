<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateWorkshopsTable extends Migration
{
	/**
	 * @return void
	 */
	public function up()
	{
		Schema::create('workshops', function (Blueprint $table) {
			$table->bigIncrements('id');
			$table->unsignedBigInteger('user_id')->nullable();
			$table->string('picture', 255)->nullable();
			$table->string('name', 255)->nullable();
			$table->unsignedBigInteger('trainer_id')->nullable();
			$table->text('description')->nullable();
			$table->dateTime('from');
			$table->dateTime('to');
			$table->unsignedDecimal('price_per_day')->nullable();
			$table->unsignedDecimal('price_total')->nullable();
			$table->text('cancellation_policy')->nullable();
			$table->timestamps();

			$table->foreign('user_id')
				->references('id')->on('users')
				->onDelete('set null');
		});

		Schema::create('workshops_orders', function (Blueprint $table) {
			$table->bigIncrements('id');
			$table->unsignedBigInteger('user_id')->nullable();
			$table->unsignedBigInteger('workshop_id')->nullable();
			$table->boolean('part_reservation')->default(false);
			$table->date('part_reservation_date')->nullable()->default(null);
			$table->boolean('paid')->default(false);
			$table->timestamps();

			$table->foreign('workshop_id')
				->references('id')->on('workshops')
				->onDelete('cascade');

			$table->foreign('user_id')
				->references('id')->on('users')
				->onDelete('set null');
		});
	}

	/**
	 * Reverse the migrations.
	 * @return void
	 */
	public function down()
	{
		Schema::table('workshops_orders', function (Blueprint $table) {
			$table->dropForeign(['workshop_id']);
			$table->dropForeign(['user_id']);
		});

		Schema::table('workshops', function (Blueprint $table) {
			$table->dropForeign(['user_id']);
		});

		Schema::dropIfExists('workshops_orders');
		Schema::dropIfExists('workshops');
	}
}
