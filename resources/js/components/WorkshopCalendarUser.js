import React, { Component } from 'react';
import FullCalendar from '@fullcalendar/react'
import dayGridPlugin from "@fullcalendar/daygrid";
import interactionPlugin from "@fullcalendar/interaction";

export default class WorkshopCalendarUser extends Component {
    constructor (props) {
        super(props)
    }

    render() {
        return (
            <FullCalendar
                defaultView="dayGridMonth"
                weekends={true}
                locale="en"
                firstDay={1}
                plugins={[ dayGridPlugin, interactionPlugin ]}
                selectable={false}
                timeFormat="H(:mm)"
                displayEventTime={false}
                events={{
                    url: '/api/workshopsPerMonthUser',
                    method: 'GET'
                }}
            />
        )
    };
}
