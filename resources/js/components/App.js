import React, { Component } from 'react'
import ReactDOM from 'react-dom'
import { BrowserRouter, Route, Switch } from 'react-router-dom'
import { Nav } from 'react-bootstrap'
import WorkshopCalendar from './WorkshopCalendar'
import WorkshopCalendarUser from './WorkshopCalendarUser'
import NewWorkshop from './NewWorkshop'

class App extends Component {
  render () {
    return (
        <BrowserRouter>
            <div>
                <Nav className="nav-pills mb-3" activeKey={location.pathname}>
                    <Nav.Link href="/">Manager</Nav.Link>
                    <Nav.Link href="/user">User</Nav.Link>
                </Nav>

                <Switch>
                    <Route exact path='/' component={WorkshopCalendar} />
                    <Route path='/create' component={NewWorkshop} />
                    <Route path='/user' component={WorkshopCalendarUser} />
                </Switch>
            </div>
        </BrowserRouter>
    )
  }
}

export default App;

ReactDOM.render(<App />, document.getElementById('app'))
