<?php

namespace App;

use Carbon\Carbon;
use Exception;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Relations\HasMany;

/**
 * Class Workshop
 * @package App
 * @mixin Builder
 */
class Workshop extends Model
{
	protected $table = 'workshops';

	protected $dates = [
		'from',
		'to',
		'created_at',
		'updated_at',
	];

	protected $fillable = [
		'name',
		'from',
		'to',
		'trainer_id',
		'description',
		'price_per_day',
		'price_total',
		'cancellation_policy',
	];



	public function workshopOrders(): HasMany
	{
		return $this->hasMany(WorkshopOrder::class);
	}



	public static function getWorkshopsForCalendar($start, $end): array
	{
		try {
			$start = new Carbon($start);
		} catch (Exception $e) {
			$start = new Carbon('first day of this month');
		}

		try {
			$start = new Carbon($start);
		} catch (Exception $e) {
			$end = new Carbon('last day of this month');
		}

		$workshops = self::where([
				['from', '>=', $start],
				['to','<', $end]
			])->withCount(['workshopOrders' => function ($query) {
				//$query->where('is_paid', false);
			}])->get();

		$now = new Carbon();
		$items = [];
		foreach($workshops as $workshop) {
			$items[] = [
				'id' => $workshop->id,
				'title' => "{$workshop->name} [{$workshop->workshop_orders_count}]",
				'start' => $workshop->from->format('Y-m-d H:i'),
				'end' => $workshop->to->format('Y-m-d H:i'),
				'url' => url("/create?id={$workshop->id}"),
				'color' => $workshop->to < $now ? 'lightgrey' : null,
			];
		}

		return $items;
	}


	public static function getWorkshopsForCalendarUser($start, $end): array
	{
		try {
			$start = new Carbon($start);
		} catch (Exception $e) {
			$start = new Carbon('first day of this month');
		}

		try {
			$start = new Carbon($start);
		} catch (Exception $e) {
			$end = new Carbon('last day of this month');
		}

		$workshops = self::where([
				['from', '>=', $start],
				['to','<', $end]
			])->withCount(['workshopOrders AS workshop_orders_not_paid' => function ($query) {
				$query->where('user_id', 1); // todo user id
				$query->where('paid', false);
			}])
			->withCount(['workshopOrders AS workshop_orders_paid' => function ($query) {
				$query->where('user_id', 1); // todo user id
				$query->where('paid', true);
			}])->get();

		$now = new Carbon();
		$items = [];
		foreach($workshops as $workshop) {
			// todo part time barvy
			if ($workshop->to < $now) {
				$color = $workshop->workshop_orders_paid ? 'lightgreen' : 'lightgrey';
			} else {
				$color = $workshop->workshop_orders_paid ? 'green' : 'black';
			}

			$items[] = [
				'id' => $workshop->id,
				'title' => $workshop->name,
				'start' => $workshop->from->format('Y-m-d H:i'),
				'end' => $workshop->to->format('Y-m-d H:i'),
				'url' => url("/registerWorkshop?id={$workshop->id}"),
				'color' => $color,
			];
		}

		return $items;
	}
}
